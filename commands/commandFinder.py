import os
import subprocess
from commands.GetCurrentDirectory import GetCurrentDirectory
from commands.ChangeDirectory import ChangeDirectory
from commands.RawCommand import RawCommand
#from commands.clearScreenCls import clearScreenCls
from pprint import pprint

import shlex


class commandFinder:


    @staticmethod
    def commands():
        # COMMANDS LIST
        commands = {

            "cd": ChangeDirectory(),
            "getcd": GetCurrentDirectory(),
            "raw": RawCommand()

        }
        # END COMMANDS LIST
        return commands


    @staticmethod
    def findCommand(RawCommand,command,socket):





        firstCommand = command[0]

        if firstCommand in commandFinder.commands():
            commandInstance = commandFinder.commands()[firstCommand]

            methodTocall = getattr(commandInstance, "executeCommand")

            print ("FULL COMMAND -> "+str(command))
            pprint ("MAIN COMMAND -> "+str(firstCommand))

            result = methodTocall(command,"")

            socket.send(str(result.encode()))
        else:
            socket.send("COMMAND NOT FOUND (CLIENT)".encode())


